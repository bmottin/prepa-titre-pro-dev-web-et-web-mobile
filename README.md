# Conseil de Preparation du Titre Professionnel __Developpeur Web et Web Mobile__

basé sur le repo https://github.com/jibundeyare/cours

## Notice 
http://www.dossierprofessionnel.fr/

[Guide TP Dev web et web mobile](titre_pro_d%C3%A9veloppeur_web_et_web_mobile.md)


## Les Docs

### Le RCV2 (Référentiel de Certification du TP)

[RCV2](main/files/RCV2_DWWM_V03_03052018.pdf)

### Le REACT (Référentiel Emploi Activité Compétences du TP)

[REACT](files/REAC_DWWM_V03_03052018.pdf)

### Le Dossier Professionnel

[Dossier Pro](files/1-Dossier_professionnel_version_traitement_de_texte_11_09_2017.docx)

